export interface List {
  _id: string;
  title: string;
  isMain?: boolean;
  date?: string;
}

export interface Task {
  _id: string;
  title: string;
  description: string;
  done: boolean;
  date: string;
}

export interface ListDialogData {
  editMode: boolean;
  title: string;
}

export interface TaskDialogData {
  editMode: boolean;
  title: string;
  description: string;
}
