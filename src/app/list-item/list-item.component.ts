import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {List} from '../interface';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class ListItemComponent implements OnInit {
  @Input() list: List;
  @Input() selected: boolean;

  constructor() {
  }

  getDate(date: string): string {
    return new Date(Date.parse(date)).toDateString();
  }

  ngOnInit() {
  }

}
