import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {TasksComponent} from './tasks/tasks.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ListComponent} from './list/list.component';
import {ListItemComponent} from './list-item/list-item.component';
import {MatListModule} from '@angular/material/list';
import {AddTaskComponent} from './add-task/add-task.component';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatRippleModule
} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {ListDialogComponent} from './list-dialog/list-dialog.component';
import {FormsModule} from '@angular/forms';
import {TaskDialogComponent} from './task-dialog/task-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    ListComponent,
    ListItemComponent,
    AddTaskComponent,
    ListDialogComponent,
    TaskDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatListModule,
    MatInputModule,
    MatIconModule,
    HttpClientModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    MatMenuModule,
    MatCheckboxModule,
    MatRippleModule,
  ],
  entryComponents: [
    ListDialogComponent,
    TaskDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
