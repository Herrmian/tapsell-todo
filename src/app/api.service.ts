import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {List, Task} from './interface';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = 'http://127.0.0.1:3000/api/';
  public mainList: List;
  constructor(private http: HttpClient) {
  }

  getMainList(): Observable<List> {
    return this.http.get<List>(this.apiUrl + 'mainList');
  }

  addList(title: string): Observable<List> {
    return this.http.post<List>(`${this.apiUrl}/lists/`, {title});
  }

  editList(id: string, title: string): Observable<List> {
    return this.http.put<List>(`${this.apiUrl}/lists/${id}`, {title});
  }

  deleteList(id: string): Observable<List> {
    return this.http.delete<List>(`${this.apiUrl}/lists/${id}`);
  }

  getLists(): Observable<List[]> {
    return this.http.get<List[]>(`${this.apiUrl}/lists/`);
  }

  getTaskLists(id: string): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.apiUrl}/tasks/query/${id}`);
  }
  getCompeletedTask(): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.apiUrl}/compeleted`);
  }

  deleteTask(id: string): Observable<Task> {
    return this.http.delete<Task>(`${this.apiUrl}/tasks/${id}`);
  }

  addTask(id: string, title: string, description: string): Observable<Task> {
    return this.http.post<Task>(`${this.apiUrl}/tasks/`, {title, list: id, description});
  }

  editTask(id: string, title: string, description: string): Observable<Task> {
    return this.http.put<Task>(`${this.apiUrl}/tasks/${id}`, {title, description});
  }

  moveTask(id: string, list: string): Observable<Task> {
    return this.http.put<Task>(`${this.apiUrl}/tasks/${id}`, {list});
  }
  doneTask(id: string, done: boolean): Observable<Task> {
    return this.http.put<Task>(`${this.apiUrl}/tasks/${id}`, {done});
  }
}
