import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class AddTaskComponent implements OnInit {
  title: string;
  @Output() submit = new EventEmitter();

  constructor() {
  }


  ngOnInit() {
  }

  onSubmit(): void {
    this.submit.emit(this.title);
    this.title = '';

  }

}
