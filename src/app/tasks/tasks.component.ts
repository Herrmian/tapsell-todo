import {Component, ViewChild} from '@angular/core';
import {Params, Router} from '@angular/router';
import {RouterParams} from '../router-params.service';
import {ApiService} from '../api.service';
import {TaskDialogData, Task, List} from '../interface';
import {MatDialog, MatMenuTrigger} from '@angular/material';
import {TaskDialogComponent} from '../task-dialog/task-dialog.component';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent {
  id: string;
  params: Params;
  tasks: Task[];
  @ViewChild(MatMenuTrigger, {static: false})
  contextMenu: MatMenuTrigger;

  contextMenuPosition = {x: '0px', y: '0px'};

  constructor(private api: ApiService, public dialog: MatDialog, private routerParams: RouterParams, private router: Router) {
    this.params = {};
    routerParams.params.subscribe(
      (params: Params): void => {
        this.params = params;
        if ('id' in this.params) {
          this.getTasks();
        }
      }
    );
  }

  getDate(date: string): string {
    return new Date(Date.parse(date)).toDateString();
  }

  onContextMenu(event: MouseEvent, task: Task) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menuData = {task};
    this.contextMenu.openMenu();
  }

  doneTask(done: boolean, task: Task): void {
    this.api.doneTask(task._id, done).subscribe(tasks => this.getTasks());
  }

  getTasks(): void {
    if (this.params.id === 'compeleted') {
      this.api.getCompeletedTask().subscribe(tasks => this.tasks = tasks);
    } else {
      this.api.getTaskLists(this.params.id).subscribe(tasks => {
          if ('error' in tasks) {
            this.router.navigateByUrl(`/tasks/${this.api.mainList._id}`);
          } else {
            this.tasks = tasks;
          }
        }
      );

    }
  }


  deleteTask(task: Task): void {
    this.api.deleteTask(task._id).subscribe(() => this.getTasks());
  }

  editTask(task: Task): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '250px',
      data: {title: task.title, description: task.description, editMode: true} as TaskDialogData
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data.title.trim()) {
        this.api.editTask(task._id, data.title, data.description).subscribe(
          list => this.getTasks()
        );
      }
    });
  }

  moveTask(task: Task): void {
    this.api.moveTask(task._id, this.api.mainList._id).subscribe(
      list => this.getTasks()
    );
  }

  addTask(): void {
    // this.api.addTask(this.params.id, title).subscribe(tasks => this.getTasks());
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '250px',
      data: {title: '', description: '', editMode: false} as TaskDialogData
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data.title ? data.title.trim() : false) {
        this.api.addTask(this.params.id, data.title, data.description).subscribe(
          list => this.getTasks()
        );
      }
    });
  }
}


