import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../api.service';
import {List, ListDialogData} from '../interface';
import {MatDialog, MatMenuTrigger} from '@angular/material';
import {ListDialogComponent} from '../list-dialog/list-dialog.component';
import {Params, Router} from '@angular/router';
import {RouterParams} from '../router-params.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  lists: List[];
  selected: string;
  params: Params;
  mainList: List;
  @ViewChild(MatMenuTrigger, {static: false})
  contextMenu: MatMenuTrigger;

  complated = {_id: 'compeleted', title: 'Compeleted'} as List;

  contextMenuPosition = {x: '0px', y: '0px'};

  constructor(private api: ApiService, public dialog: MatDialog, private routerParams: RouterParams, private router: Router) {
    this.params = {};
    routerParams.params.subscribe(
      (params: Params): void => {
        this.params = params;
      }
    );
  }


  onContextMenu(event: MouseEvent, list: List) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menuData = {list};
    if (list._id !== this.mainList._id && list._id !== 'compeleted' ) {
      this.contextMenu.openMenu();
    }
  }

  ngOnInit() {

    this.api.getMainList().subscribe(
      list => {
        this.mainList = list;
        this.api.mainList = list;
        if (!('id' in this.params)) {
          this.router.navigateByUrl(`/tasks/${this.mainList._id}`);
        }
        this.getList();
      }
    );
    this.getList();
  }

  getList(): void {
    this.api.getLists().subscribe(lists => this.lists = lists);
  }

  deleteList(list: List): void {
    this.api.deleteList(list._id).subscribe(
      data => this.getList()
    );
  }

  editList(list: List): void {
    const dialogRef = this.dialog.open(ListDialogComponent, {
      width: '250px',
      data: {title: list.title, editMode: true} as ListDialogData,
    });

    dialogRef.afterClosed().subscribe(title => {
      if (title !== list.title) {
        this.api.editList(list._id, title).subscribe(
          data => this.getList()
        );
      }
    });
  }

  addList(): void {
    const dialogRef = this.dialog.open(ListDialogComponent, {
      width: '250px',
      data: {title: '', editMode: false} as ListDialogData,
    });

    dialogRef.afterClosed().subscribe(title => {
      if (title) {
        this.api.addList(title).subscribe(
          list => this.getList()
        );
      }
    });
  }

}
